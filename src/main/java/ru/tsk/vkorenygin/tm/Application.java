package ru.tsk.vkorenygin.tm;

import ru.tsk.vkorenygin.tm.api.ICommandRepository;
import ru.tsk.vkorenygin.tm.constant.ArgumentConst;
import ru.tsk.vkorenygin.tm.constant.CommandConst;
import ru.tsk.vkorenygin.tm.model.Command;
import ru.tsk.vkorenygin.tm.repository.CommandRepository;
import ru.tsk.vkorenygin.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

	private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

	public static void main(String[] args) {
		System.out.println("-- WELCOME TO TASK MANAGER --");
		parseArgs(args);
		final Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("ENTER COMMAND:");
			final String command = scanner.nextLine();
			parseCommand(command);
		}
	}

	public static void parseCommand(String command) {
		switch (command) {
			case CommandConst.ABOUT:
				showAbout();
				break;
			case CommandConst.VERSION:
				showVersion();
				break;
			case CommandConst.INFO:
				showInfo();
				break;
			case CommandConst.HELP:
				showHelp();
				break;
			case CommandConst.COMMANDS:
				showCommands();
				break;
			case CommandConst.ARGUMENTS:
				showArguments();
				break;
			case CommandConst.EXIT:
				exit();
				break;
			default:
				showErrorCommand();
		}
	}

	private static void showErrorCommand() {
		System.err.println("Error! Command not found.");
	}

	public static void parseArgs(String[] args) {
		if (args == null || args.length == 0)
			return;
		final String arg = args[0];
		parseArg(arg);
		System.exit(0);
	}

	public static void parseArg(String arg) {
		switch (arg) {
			case ArgumentConst.ABOUT:
				showAbout();
				break;
			case ArgumentConst.VERSION:
				showVersion();
				break;
			case ArgumentConst.INFO:
				showInfo();
				break;
			case ArgumentConst.HELP:
				showHelp();
				break;
			default:
				showArgumentError();
		}
	}

	private static void showArgumentError() {
		System.err.println("Error! Argument not supported.");
		System.exit(1);
	}

	public static void showAbout() {
		System.out.println("- ABOUT -");
		System.out.println("Developed by: Vladimir Korenyugin");
		System.out.println("E-mail: vkorenygin@tsconsulting.com");
	}

	public static void showVersion() {
		System.out.println("- VERSION -");
		System.out.println("1.0.0");
	}

	public static void showInfo() {

		System.out.println("- INFO -");

		final int availableProcessors = Runtime.getRuntime().availableProcessors();
		System.out.println("Available processors (cores): " + availableProcessors);

		final long freeMemory = Runtime.getRuntime().freeMemory();
		System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));

		final long maxMemory = Runtime.getRuntime().maxMemory();
		final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
		final String maxMemoryFormat =
				(maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
		System.out.println("Maximum memory: " + maxMemoryFormat);

		final long totalMemory = Runtime.getRuntime().totalMemory();
		System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));

		final long usedMemory = totalMemory - freeMemory;
		System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));

	}

	public static void showCommands() {
		System.out.println("- COMMANDS -");
		Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
		for (final Command command : commands) {
			showCommandValue(command.getName());
		}
	}

	public static void showArguments() {
		System.out.println("- ARGUMENTS -");
		Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
		for (final Command command : commands) {
			showCommandValue(command.getArgument());
		}
	}

	private static void showCommandValue(final String value) {
		if (value == null || value.isEmpty())
			return;
		System.out.println(value);
	}

	public static void showHelp() {
		System.out.println("- HELP -");
		Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
		for (final Command command : commands) System.out.println(command);
	}

	public static void exit() {
		System.exit(0);
	}
}